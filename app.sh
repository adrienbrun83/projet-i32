#!/bin/sh

echo "-----------------------"
echo "| <Le nom de l'appli> |"
echo "-----------------------"
echo ""
echo "[0]: Gerer le materiel"
echo "[1]: Gerer les bateaux"
echo "[2]: Gerer les locations"
echo "[3]: Gerer les sorties"
echo "[4]: Gerer les plongeurs"
echo "[5]: Gerer les palanquees"
echo "[6]: Gerer les reservations"

read choix

case $choix in
	"0")
		echo "Vous avez choisi 0"
		;;
	"1")
		echo "Vous avez choisi 1"
		;;
	"2")
		echo "Vous avez choisi 2"
		;;
	"3")
		echo "Vous avez choisi 3"
		;;
	"4")
		echo "Vous avez choisi 4"
		;;
	"5")
		echo "Vous avez choisi 5"
		;;
	"6")
		echo "Vous avez choisi 6"
		;;
	*)
		echo "Veuillez entrer un nombre valide"
		;;
esac
