from modele import *

class MaterielSecours(Modele):
    """la classe des materiel de Secours de Plongee obligatoire (nomMateriel, taille, prixMateriel, quantite)"""

    def __init__(self, nomMateriel, taille, prixMateriel, quantite):
        self.nomMateriel = nomMateriel
        self.taille = taille
        self.prixMateriel = prixMateriel
        self.quantite = quantite
    def __str__(self):
        return "Materiel: " + str(self.nomMateriel) + ", taille: " + str(self.taille) +", prix: "+str(self.prixMateriel)+", quantite: "+str(self.quantite)


