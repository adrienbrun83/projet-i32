from modele import *
from database import db

class Materiel(Modele):
    """la classe du materiel de Plongee (nomMateriel, taille, prixMateriel, quantite)"""

    def __init__(self, nomMateriel, taille, prixMateriel, quantite):
        self.nomMateriel = nomMateriel
        self.taille = taille
        self.prixMateriel = prixMateriel
        self.quantite = quantite

    def __str__(self):
        return "Materiel: " + str(self.nomMateriel) + ", taille: " + str(self.taille) +", prix: "+str(self.prixMateriel)+", quantite: "+str(self.quantite)

    @staticmethod
    def get_all():
        #SELECT * FROM materiel;
        result = db.execute_and_fetch("SELECT * FROM materiel;")
        return list(map(Materiel.from_tuple, result))

    @staticmethod
    def get(nomMateriel,taille):
        #SELECT * FROM materiel WHERE nomMateriel=nomMateriel AND taille=taille;
        result = db.execute_and_fetch("SELECT * FROM materiel WHERE nomMateriel = %s AND taille = %s;", (nomMateriel,taille))[0]
        return Materiel.from_tuple(result) # On transforme un tuple en matos

    def insert(self):
        #INSERT INTO materiel  (nomMateriel, taille, prixMateriel, quantite)
        #VALUES (self.nomMateriel, self.taille, self.prixMateriel, self.quantite );
        db.execute("INSERT INTO materiel (nomMateriel, taille, prixMateriel, quantite) VALUES (%s, %s, %s, %s);", (self.nomMateriel, self.taille, self.prixMateriel, self.quantite))
        db.commit()

    def update(self):
        #UPDATE materiel
        #SET prixMateriel=self.prixMateriel, quantite = self.quantite
        #WHERE nomMateriel = self.nomMateriel AND taille = self.taille;
        db.execute("""UPDATE materiel
        SET prixMateriel = %s, quantite = %s
        WHERE nomMateriel = %s AND taille = %s ;""", (self.prixMateriel, self.quantite, self.nomMateriel, self.taille))
        db.commit()

    def remove(self):
        #DELETE FROM materiel WHERE nomMateriel = self.nomMateriel AND taille = self.taille;
        db.execute("DELETE FROM materiel WHERE nomMateriel = %s AND taille = %s;", (self.nomMateriel,self.taille,))
        db.commit()

    @staticmethod
    def from_tuple(t):
        if len(t) != 4: raise TupleException("Le tuple ne contient pas autant de données que la class Materiel")
        l = list(t)
        m = Materiel(l[0], l[1], l[2], l[3])
        return m
