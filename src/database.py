import psycopg2

class Database(object):
    def __init__(self, dbName, dbUser = "postgres", dbPassword = "", dbHost = "localhost"):
        self.connection = psycopg2.connect("dbname=" + dbName + " user=" + dbUser)

    def execute(self, request, data = ()):
        cur = self.connection.cursor()
        cur.execute(request, data)
        cur.close()

    def execute_and_fetch(self, request, data = ()):
        cur = self.connection.cursor()
        cur.execute(request, data)
        result = cur.fetchall()
        cur.close()
        return result

    def commit(self):
        self.connection.commit()

    def close(self):
        self.connection.close()

db = Database("centre_plongee")
