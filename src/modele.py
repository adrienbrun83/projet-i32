
class Modele(object):
    "La classe modele de base"
    @staticmethod
    def get_all():
        pass

    @staticmethod
    def get(params):
        pass

    def insert(self):
        pass

    def update(self, params):
        pass

    def remove(self):
        pass

    @staticmethod
    def from_tuple(t):
        pass

class TupleException(Exception):
    "Levee si l'on cherche a convertir un tuple en entite alors qu'il ne correspond pas"
    def __init__(self, msg):
        self.msg = msg
