from modele import *

class Bouteille(Modele):
    """Les bouteilles de gaz"""

    def __init__(self, volume, materiaux, configuration, gaz):
        self.volume = volume
        self.materiaux = materiaux
        self.configuration = configuration
        self.gaz = gaz
    def __str__(self):
        return "volume: " + str(self.volume) + ", materiaux: " + str(self.materiaux) +", configuration: "+str(self.configuration)+", gaz: "+str(self.gaz)

