from modele import *
from database import db

class Bateau(Modele):
    """la classe des bateaux (nomBateau, capacite, estDisponible)"""

    def __init__(self, nomBateau, capacite, estDisponible):
        self.nomBateau = nomBateau
        self.capacite = capacite
        self.estDisponible = estDisponible
    def __str__(self):
        return "Bateau: " + str(self.nomBateau) + ", capacite: " + str(self.capacite) +", disponible: "+str(self.estDisponible)

    @staticmethod
    def get_all():
        result = db.execute_and_fetch("SELECT * FROM bateau;")
        return list(map(Bateau.from_tuple, result))

    @staticmethod
    def get(nomBateau):
        result = db.execute_and_fetch("SELECT * FROM bateau WHERE nomBateau = %s;", (nomBateau,))[0] # On extrait le seul element de la liste
        return Bateau.from_tuple(result) # On transforme un tuple en bateau

    def insert(self):
        db.execute("INSERT INTO bateau (nomBateau, capacite, estDisponible) VALUES (%s, %s, %s);", (self.nomBateau, self.capacite,self.estDisponible))
        db.commit()

    def update(self):
        db.execute("""UPDATE bateau
        SET capacite = %s, estDisponible = %s
        WHERE nomBateau = %s;""", (self.capacite, self.estDisponible, self.nomBateau))
        db.commit()

    def remove(self):
        db.execute("DELETE FROM bateau WHERE nomBateau = %s;", (self.nomBateau,))
        db.commit()

    @staticmethod
    def from_tuple(t):
        if len(t) != 3: raise TupleException("Le tuple ne contient pas autant de données que la class Bateau")
        l = list(t)
        b = Bateau(l[0], l[1], l[2])
        return b

