from modele import *

class Prestation(Modele):
    """Prestation (formation/exploration)"""

    def __init__(self, typePrestation, prix):
        self.typePrestation = typePrestation
        self.prix = prix
    def __str__(self):
        return "Type: " + self.typePrestation + ", prix: " + str(self.prix)


