from bateau import *
from database import db

class App(object):
    """L'application"""

    def __init__(self):
        pass

    def executer(self):
        db.close()

    def afficher_reservations(self):
        pass

    def ajouter_reservation(self):
        pass

    def afficher_materiel_disponible(self):
        pass

    def afficher_plongeurs(self):
        pass

    def afficher_bateaux_disponibles(self):
        pass

    def afficher_paanquees(self):
        pass

    def ajouter_palanquee(self):
        pass

    def supprimmer_palanquee(self):
        pass

    def ajouter_location(self):
        pass

    def supprimmer_location(self):
        pass

    def ajouter_sortie(self):
        pass

    def supprimmer_sortie(self):
        pass


app = App()
app.executer()
