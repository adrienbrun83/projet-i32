from modele import *

class Palanquee(Modele):
    """la classe des sites de Plongee (numPalanquee, moniteur)"""

    def __init__(self, numPalanquee, moniteur):
        self.numPalanquee = numPalanquee
        self.moniteur = moniteur
    def __str__(self):
        return "palanquee: " + str(self.numPalanquee) + ", moniteur: " + str(self.moniteur)

