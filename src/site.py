from modele import *

class Site(Modele):
    """la classe des sites de Plongee (nomSite, typeSite, coordonnees, supplement, prerogativeMinPE)"""

    def __init__(self, nomSite, typeSite, coordonnees, supplement, prerogativeMinPE):
        self.nomSite = nomSite
        self.typeSite = typeSite
        self.coordonnees = coordonnees
        self.supplement = supplement
        self.prerogativeMinPE = prerogativeMinPE
    def __str__(self):
        return "Site: " + str(self.nomSite) + ", type: " + str(self.typeSite) +", coordonnees: "+str(self.coordonnees)+", supplement: "+str(self.supplement)+", prerogativeMinPE: "+str(self.prerogativeMinPE)


