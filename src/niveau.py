from modele import *

class Niveau(Modele):
    """La classe niveaux des plongeurs (numNiveau,organisme,profondeurPE,profondeurPA)"""

    def __init__(self, numNiveau, organisme, profondeurPE, profondeurPA):
        self.numNiveau = numNiveau
        self.organisme = organisme
        self.profondeurPE = profondeurPE
        self.profondeurPA = profondeurPA
    def __str__(self):
        return "Niveau: " + str(self.numNiveau) + ", organisme: " + str(self.organisme) +", profondeurPE: "+str(self.organisme)+", profondeurPA: "+str(self.profondeurPA)


