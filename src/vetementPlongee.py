from modele import *

class VetementPlongee(Modele):
    """la classe des vetement de Plongee (nomVetement, taille, prixVetement, quantite)"""

    def __init__(self, nomVetement, taille, prixVetement, quantite):
        self.nomVetement = nomVetement
        self.taille = taille
        self.prixVetement = prixVetement
        self.quantite = quantite
    def __str__(self):
        return "Vetement: " + str(self.nomVetement) + ", taille: " + str(self.taille) +", prix: "+str(self.prixVetement)+", quantite: "+str(self.quantite)


